QEMU Python Tooling
===================

This package serves as the core of the QEMU PEP420 Python namespace
package. It provides QEMU tooling used by the QEMU project to build,
configure, and test QEMU. It is not a fully-fledged SDK and it is
subject to change at any time.


Usage
-----

Installing the ``qemu`` package will pull in individual
subpackages. ``qemu.qmp`` is currently the only published subpackage.

The ``qemu.qmp`` subpackage provides a library for communicating with
QMP and QEMU Guest Agent servers. Refer to the package's documentation
(``>> help(qemu.qmp)``) for more information.


Versioning and Dependencies
---------------------------

The QEMU namespace indicates various subpackages and dependencies to be
installed, these versions will never be pinned or constrained in any
way. For projects needing to consume specific versions, it's recommended
to pin against the subpackages directly. The namespace need not be
installed; it's not a dependency of any of the subpackages.

This namespace will carry its own version; each change to the set of
recommended subpackages will increment the minor version (x.Y.z) by
one. Changes that don't affect dependency resolution will bump the micro
version.

Unforeseen changes to this versioning scheme will be introduced at
version 1.0.0, if necessary.


Contributing
------------

This package is in an early alpha state, so the contribution mechanisms
are still to be determined. Stay tuned!
