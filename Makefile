.PHONY: help
help:
	@echo "python packaging help:"
	@echo ""
	@echo "make clean:"
	@echo "    Remove package build output."
	@echo ""
	@echo "make distclean:"
	@echo "    remove build/, dist/, and egg-info files."
	@echo ""
	@echo "make dist:"
	@echo "    create distribution files in dist/"
	@echo ""
	@echo "make publish-test:"
	@echo "    Dry run; publish this package."
	@echo ""
	@echo "make publish:"
	@echo "    Build and publish this package!"
	@echo ""
	@echo -e "Have a nice day ^_^\n"

.PHONY: clean
clean:
	python3 setup.py clean --all

.PHONY: distclean
distclean: clean
	rm -rf qemu.egg-info/ build/ dist/

.PHONY: pristine
pristine:
	@git diff-files --quiet --ignore-submodules -- || \
		(echo "You have unstaged changes."; exit 1)
	@git diff-index --cached --quiet HEAD --ignore-submodules -- || \
		(echo "Your index contains uncommitted changes."; exit 1)
	@[ -z "$(shell git ls-files -o)" ] || \
		(echo "You have untracked files: $(shell git ls-files -o)"; exit 1)

dist: setup.cfg setup.py Makefile README.rst
	python3 -m build

.PHONY: publish
publish: pristine dist
	python3 -m twine check --strict dist/*
	git push -v --atomic --follow-tags --dry-run
	# Set the username via TWINE_USERNAME.
	# Set the password via TWINE_PASSWORD.
	# Set the repository via TWINE_REPOSITORY.
	python3 -m twine upload --verbose dist/*
	git push -v --atomic --follow-tags

.PHONY: publish-test
publish-test: pristine dist
	python3 -m twine check --strict dist/*
	git push -v --atomic --follow-tags --dry-run
	python3 -m twine upload --verbose --repository-url 'https://test.pypi.org/legacy/' dist/*
